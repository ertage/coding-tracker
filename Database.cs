using System;
using System.Configuration;
using System.Data.SQLite;
using ConsoleTableExt;

namespace coding_tracker
{
    class Database
    {
    private TimeHandler th = new TimeHandler();
    private string connectionString = ConfigurationManager.ConnectionStrings["CodingTracker"].ConnectionString;  
    private List<DataList> showList = new List<DataList>();

    public void CreateDatabase()  
    {  
        using (SQLiteConnection connection = new SQLiteConnection(connectionString))  
        {  
            using (SQLiteCommand command = new SQLiteCommand(connection))
            {
                connection.Open();
                command.CommandText = @"CREATE TABLE IF NOT EXISTS tracker (ID INTEGER PRIMARY KEY AUTOINCREMENT, 
                                                                            name VARCHAR(20), 
                                                                            starttime TEXT, 
                                                                            stoptime TEXT, 
                                                                            duration TEXT)";
                command.ExecuteNonQuery();
                connection.Close();
            }
        }
    }
    public void ManualDataEntry()
    {
        string? name = NameEntry();
        string startTime = th.CodingStartTime();
        string stopTime = th.CodingStopTime();
        string duration = th.CodingDuration(startTime,stopTime);
        AddDataEntry(name, startTime, stopTime, duration);
    }
    public void AutomaticDataEntry()
    {
        string? name = NameEntry();
        Console.WriteLine("Press any key to start");
        Console.ReadKey();
        string startWatch = th.GetTimeNow(); 
        string duration = th.TrackerWatch();
        string stopWatch = th.GetTimeNow();
        Console.ReadKey();
        Console.WriteLine("Duration:" + duration);
        AddDataEntry(name, startWatch, stopWatch, duration);

    }

    public void AddDataEntry(string? nameEntry, string start, string stop, string duration)
    {

        using (SQLiteConnection connection = new SQLiteConnection(connectionString))  
        {  
            using (SQLiteCommand command = new SQLiteCommand(connection))
            {
                connection.Open();
                command.CommandText = @"INSERT INTO tracker (name, starttime, stoptime, duration) 
                                        VALUES(@name, @start, @stop, @dura)";
                command.Parameters.Add(new SQLiteParameter("@name", nameEntry));
                command.Parameters.Add(new SQLiteParameter("@start", start));
                command.Parameters.Add(new SQLiteParameter("@stop", stop));
                command.Parameters.Add(new SQLiteParameter("@dura", duration));
                command.ExecuteNonQuery();
                connection.Close();
                Console.WriteLine("Press any key to return to Menu");
                Console.ReadKey();
                

            }
        }
    }
    public void EditData(string? editChoice)
    {
        string startTime;
        string stopTime;
        string duration;
        int id;
        if(editChoice == "M")
        {
            Console.Write("Enter ID of the entry you want to change:");
            id = IdEntry();
            startTime = th.CodingStartTime();
            stopTime = th.CodingStopTime();
            duration = th.CodingDuration(startTime,stopTime);
        }
        else if (editChoice == "A")
        {
            Console.Write("Enter ID of the entry you want to change:");
            id = IdEntry();
            Console.WriteLine("Press any key to start");
            Console.ReadKey();
            startTime = th.GetTimeNow(); 
            duration = th.TrackerWatch();
            stopTime = th.GetTimeNow();
            Console.WriteLine("Duration:" + duration);
        }
        else
        {
            Console.WriteLine("Please enter either M or A");
            return;
        }
        using (SQLiteConnection connection = new SQLiteConnection(connectionString))  
        {  
            using (SQLiteCommand command = new SQLiteCommand(connection))
            {
            connection.Open();
            command.CommandText = @"SELECT count(*) FROM tracker WHERE ID = (@id)";
            command.Parameters.Add(new SQLiteParameter("@id", id));
            command.Parameters.Add(new SQLiteParameter("@newStart", startTime));
            command.Parameters.Add(new SQLiteParameter("@newStop", stopTime));
            command.Parameters.Add(new SQLiteParameter("@newDuration", duration));
            object result = command.ExecuteScalar();
            int resultCount = Convert.ToInt32(result);
            if (resultCount == 1)
            {
                command.CommandText = @"UPDATE tracker SET (STARTTIME, STOPTIME, DURATION) = (@newStart, @newStop, @newDuration) WHERE ID = (@id)";
            }
            else
            {
                Console.WriteLine("This ID doesn't exist, try another one");
            }
            command.ExecuteNonQuery();
            connection.Close();
            }

        }
    }

    public void DeleteData()
    {
        Console.Write("Choose ID of entry you want to delete:");
        int id = IdEntry();
        using (SQLiteConnection connection = new SQLiteConnection(connectionString))  
        {  
            using (SQLiteCommand command = new SQLiteCommand(connection))
            {
            connection.Open();
            command.CommandText = @"SELECT count(*) FROM tracker WHERE ID = (@id)";
            command.Parameters.Add(new SQLiteParameter("@id", id));
            object result = command.ExecuteScalar();
            int resultCount = Convert.ToInt32(result);
            if (resultCount == 1)
            {
            command.CommandText = @"DELETE FROM tracker WHERE ID = (@id)";
            Console.WriteLine("You deleted entry #{0}", id);
            }
            else
            {
                Console.WriteLine("This ID doesn't exist, try another one");
            }
            command.ExecuteNonQuery();
            Console.WriteLine("Press any key to return to Menu");
            Console.ReadKey();
            }
        }
    }

    public void ShowData()
    {

        using (SQLiteConnection connection = new SQLiteConnection(connectionString))  
        {  
            using (SQLiteCommand command = new SQLiteCommand(connection))
            {
                connection.Open();
                command.CommandText = @"SELECT count(*) FROM tracker";
                object result = command.ExecuteScalar();
                int resultCount = Convert.ToInt32(result);
                if (resultCount > 0)
                {
                    string output = @"SELECT * FROM tracker";
                    SQLiteCommand command1 = new SQLiteCommand(output, connection);
                    SQLiteDataReader reader = command1.ExecuteReader();
                    while (reader.Read() == true)
                    {
                        //showList.Add(new List<object> { reader["ID"], reader["name"], reader["starttime"], reader["stoptime"], reader["duration"]});
                        showList.Add(new DataList() {id = reader.GetInt32(0), name = reader["name"].ToString(), startTime = reader["starttime"].ToString(), 
                        stopTime = reader["stoptime"].ToString(), duration = reader["duration"].ToString()});
                    }
                }
                else
                {
                    Console.WriteLine("There are no entries yet");
                }
            }
            ConsoleTableBuilder
            .From(showList)
            .ExportAndWrite();
            Console.WriteLine();
            Console.WriteLine("Press any key to return to Menu");
            Console.ReadKey();
        }

    }

    /*
    User Input
    */
    public string? NameEntry()
    {
        Console.WriteLine("Enter entry name:");
        var nameEntry = Console.ReadLine();
        return nameEntry;
    }

    public int IdEntry()
    {
        string? Input = Console.ReadLine();
        int id;
        while(!int.TryParse(Input, out id) || id < 0)
        {
            Console.Write("Not a valid input, try again:");
            Input = Console.ReadLine();
        }
        return id;
    }

    }
}