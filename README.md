## Coding Tracker

Console based CRUD application developed using C# and SQLite.
Helps you to track time spent on coding.
Either with a built in stopwatch or manually adding the times.

## Features

- Manual entry of coding times
- Stop Watch with automatic entry of coding time
- Edit, Delete, Add new entries
- Show all entries

## Sample Screenshots

![Main Menu](images/menu.png)

![Entry View](images/showentries.png)

## Used Packages

 - [ConsoleTableExt](https://github.com/minhhungit/ConsoleTableExt)
 - [Connection String](https://docs.microsoft.com/en-us/dotnet/api/system.configuration.configurationmanager.connectionstrings?view=dotnet-plat-ext-6.0)
 - [SQLClient](https://docs.microsoft.com/en-us/dotnet/api/system.data.sqlclient?view=windowsdesktop-6.0)
