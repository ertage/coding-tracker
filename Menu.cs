using System;

namespace coding_tracker
{
    class Menu
    {

        private bool check = true;
        public void MenuPrompt()
        {
            while(check)
            {
            Database db = new Database();
            Console.WriteLine("MENU");
            Console.WriteLine("(M)anual Entry (Enter the coding time)");
            Console.WriteLine("(A)utomatic Entry (Stopwatch keeps track of coding time)");
            Console.WriteLine("(E)dit Entry");
            Console.WriteLine("(S)how Entries");
            Console.WriteLine("(D)elete Entry");
            Console.WriteLine("(Q)uit Application");
            Console.Write("Choose an Option:");
            string? menuOption = Console.ReadLine().ToUpper();

            switch(menuOption)
            {
                case "M":
                    db.ManualDataEntry();
  			  	break;
				case "A":
                    db.AutomaticDataEntry();
  				break;
				case "E":
                    Console.WriteLine("Do you want to edit the entry (M)anually or (A)utomatic?");
                    string? choice = Console.ReadLine().ToUpper();
                    db.EditData(choice);
				break;
				case "S":
                    db.ShowData();
				break;
				case "D":
                    db.DeleteData();
				break;
				case "Q":
                    Environment.Exit(0);
				break;
  				default:
				  Console.WriteLine("Not a Valid Input! Try again!");
  				break;
			}
            }

        }

    }
}