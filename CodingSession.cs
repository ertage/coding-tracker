using System;

namespace coding_tracker
{
    public class DataList
    {
        public int id {get; set;}
        public string? name {get; set;}
        public string? startTime {get; set;}
        public string? stopTime {get; set;}
        public string? duration {get; set;}
    }

}