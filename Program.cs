﻿using System;  
using System.Configuration;  
using System.Data.SQLite;  

namespace coding_tracker  
{  
    class Program  
    {  
        static void Main(string[] args)  
        {  
            Database db = new Database();
            db.CreateDatabase();
            Menu menu = new Menu();
            menu.MenuPrompt();
        }  

    }  
}