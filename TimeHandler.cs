using System;  
using System.Configuration;  
using System.Data.SQLite;  
using System.Globalization;
using System.Diagnostics;

namespace coding_tracker  
{  

    class TimeHandler
    {  
        private DateTime tempTime;
        private CultureInfo cultureInfo = new CultureInfo("de-DE");
        private DateTimeStyles dateTimeStyles = new DateTimeStyles();

        //Manual starting date + time entry
        public string CodingStartTime()
        {
            Console.WriteLine("Enter coding start time: DD.MM.YY HH:mm");
            string? startTime = Console.ReadLine();
            while (!DateTime.TryParseExact(startTime,"dd.MM.yy HH:mm", cultureInfo, dateTimeStyles, out tempTime))
            {
                Console.WriteLine("Wrong Format, try again!");
                startTime = Console.ReadLine();
            }
            return startTime;
        }

        //Manual stopping date + time entry
        public string CodingStopTime()
        {
            Console.WriteLine("Enter coding stop time: DD.MM.YY HH:mm");
            string? startTime = Console.ReadLine();
            while (!DateTime.TryParseExact(startTime,"dd.MM.yy HH:mm", cultureInfo, dateTimeStyles, out tempTime))
            {
                Console.WriteLine("Wrong Format, try again!");
                startTime = Console.ReadLine();
            }
            return startTime;
        }

        //Duration Calculator
        public string CodingDuration(string startTime, string stopTime)
        {
            DateTime a = DateTime.Parse(startTime, cultureInfo);
            DateTime b = DateTime.Parse(stopTime, cultureInfo);
            TimeSpan duration = b - a;
            string dura = duration.ToString();
            return dura;
        }

        //Stopwatch to meassure coding time
        public string TrackerWatch()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Console.WriteLine("Press any key to stop");
            while(!Console.KeyAvailable)
            {
            Console.Write(String.Format("\r{0:00}:{1:00}:{2:00}", sw.Elapsed.Hours, sw.Elapsed.Minutes, sw.Elapsed.Seconds));
            System.Threading.Thread.Sleep(1000);
            }
            Console.WriteLine("");
            sw.Stop();
            TimeSpan ts = sw.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}",
            ts.Hours, ts.Minutes, ts.Seconds);
            return elapsedTime;
        }

        //Get the current date + time
        public string GetTimeNow()
        {
            DateTime tempNow = DateTime.Now;
            string timeNow = tempNow.ToString("dd.MM.yy hh:mm");
            return timeNow;
        }

    }  
}
